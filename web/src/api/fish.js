import service from '@/utils/request'

// @Tags Fish
// @Summary 创建Fish
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Fish true "创建Fish"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /fish/createFish [post]
export const createFish = (data) => {
     return service({
         url: "/fish/createFish",
         method: 'post',
         data
     })
 }


// @Tags Fish
// @Summary 删除Fish
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Fish true "删除Fish"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /fish/deleteFish [delete]
 export const deleteFish = (data) => {
     return service({
         url: "/fish/deleteFish",
         method: 'delete',
         data
     })
 }

// @Tags Fish
// @Summary 更新Fish
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Fish true "更新Fish"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /fish/updateFish [put]
 export const updateFish = (data) => {
     return service({
         url: "/fish/updateFish",
         method: 'put',
         data
     })
 }


// @Tags Fish
// @Summary 用id查询Fish
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Fish true "用id查询Fish"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /fish/findFish [get]
 export const findFish = (params) => {
     return service({
         url: "/fish/findFish",
         method: 'get',
         params
     })
 }


// @Tags Fish
// @Summary 分页获取Fish列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.PageInfo true "分页获取Fish列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /fish/getFishList [get]
 export const getFishList = (params) => {
     return service({
         url: "/fish/getFishList",
         method: 'get',
         params
     })
 }

 export const findFishpond = (params) => {
    return service({
        url: "/fish/findFishpond?id="+params,//?id=+params,
        method: 'get',
    })
}