package router

import (
	v1 "gin-vue-admin/api/v1"
	"github.com/gin-gonic/gin"
)

//func InitFishRouter(Router *gin.RouterGroup) {
//	FishRouter := Router.Group("fish").Use(middleware.JWTAuth()).Use(middleware.CasbinHandler())
//	{
//		FishRouter.POST("createFish", v1.CreateFish)   // 新建Fish
//		FishRouter.DELETE("deleteFish", v1.DeleteFish) // 删除Fish
//		FishRouter.PUT("updateFish", v1.UpdateFish)    // 更新Fish
//		FishRouter.GET("findFish", v1.FindFish)        // 根据ID获取Fish
//		FishRouter.GET("getFishList", v1.GetFishList)  // 获取Fish列表
//		FishRouter.GET("findFishpond", v1.FindFishpond)  // 获取对应FishPond列表
//	}
//}
func InitFishRouter(Router *gin.RouterGroup) {
	FishRouter := Router.Group("fish")
	{
		FishRouter.POST("createFish", v1.CreateFish)   // 新建Fish
		FishRouter.DELETE("deleteFish", v1.DeleteFish) // 删除Fish
		FishRouter.PUT("updateFish", v1.UpdateFish)    // 更新Fish
		FishRouter.GET("findFish", v1.FindFish)        // 根据ID获取Fish
		FishRouter.GET("getFishList", v1.GetFishList)  // 获取Fish列表
		FishRouter.GET("findFishpond", v1.FindFishpond)  // 获取对应FishPond列表
		FishRouter.GET("findTime", v1.GetFishPondListByTime)  // 获取对应FishPond列表
	}
}