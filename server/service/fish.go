package service

import (
	"bytes"
	"fmt"
	"gin-vue-admin/global"
	"gin-vue-admin/model"
	"gin-vue-admin/model/request"
	"net/http"
	"strconv"
)

// @title    CreateFish
// @description   create a Fish
// @param     fish               model.Fish
// @auth                     （2020/04/05  20:22）
// @return    err             error

func CreateFish(fish model.Fish) (err error) {
	err = global.GVA_DB.Create(&fish).Error
	return err
}

// @title    DeleteFish
// @description   delete a Fish
// @auth                     （2020/04/05  20:22）
// @param     fish               model.Fish
// @return                    error

func DeleteFish(fish model.Fish) (err error) {
	err = global.GVA_DB.Delete(fish).Error
	return err
}

// @title    UpdateFish
// @description   update a Fish
// @param     fish          *model.Fish
// @auth                     （2020/04/05  20:22）
// @return                    error

func Post(devID int,choose string) bool {
	text0:=[]byte(choose)
	url:="http://api.heclouds.com/cmds?device_id="+strconv.Itoa(devID)
	req, _ := http.NewRequest("POST",url,bytes.NewBuffer(text0))
	req.Header.Set("api-key", "Lf=fP002oXgQzlbuY=AOjh7biA8=")
	resp, _ := (&http.Client{}).Do(req)
	defer resp.Body.Close()
	return true
}

func UpdateFish(fish *model.Fish) (err error) {
	err = global.GVA_DB.Save(fish).Error
	if err==nil&&fish.Status==true{
		fmt.Println("打开增氧机！")
		Post(605690246,"D10")
	}else if err==nil&&fish.Status==false{
		fmt.Println("关闭增氧机！")
		Post(605690246,"D11")
	}
	return err
}

// @title    GetFish
// @description   get the info of a Fish
// @auth                     （2020/04/05  20:22）
// @param     id              uint
// @return                    error
// @return    Fish        Fish

func GetFish(id int,tableName string) ( error, model.Fish) {
	var fish model.Fish
	err := global.GVA_DB.Table(tableName).Where("id = ?", id).First(&fish).Error
	fmt.Println("This is Get Fish : ",fish,id ,tableName)
	return err,fish
}

func GetFishpond(id int) (err error, fish model.Fish) {
	err = global.GVA_DB.Table("fishpond1").Where("id = ?", id).First(&fish).Error
	return
}

func GetFishPondInfoByTime(id int) (err error, fish[] model.Fish) {
	index:=strconv.Itoa(id)
	tableName:="fishpond"+index
	fmt.Println("tableName",tableName)
	err = global.GVA_DB.Table(tableName).Limit(7).Find(&fish).Error
	return
}
// @title    GetFishInfoList
// @description   get Fish list by pagination, 分页获取用户列表
// @auth                     （2020/04/05  20:22）
// @param     info            PageInfo
// @return                    error

func GetFishInfoList(info request.FishSearch) (err error, list interface{}, total int) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
    // 创建db
	db := global.GVA_DB.Model(&model.Fish{})
    var fishs []model.Fish
    // 如果有条件搜索 下方会自动创建搜索语句
    //if !info.Update_time.IsZero() {
    //     db = db.Where("update_time = ?",info.Update_time)
    //}
    if info.Temp0 != 0 {
        db = db.Where("temp0 = ?",info.Temp0)
    }
    if info.Bmp != 0 {
        db = db.Where("bmp = ?",info.Bmp)
    }
    if info.Hum != 0 {
        db = db.Where("hum = ?",info.Hum)
    }
    if info.Temp1 != 0 {
        db = db.Where("temp1 = ?",info.Temp1)
    }
    //if info.Status != nil {
    //    db = db.Where("status = ?",info.Status)
    //}
	err = db.Count(&total).Error
	err = db.Limit(limit).Offset(offset).Find(&fishs).Error
	return err, fishs, total
}