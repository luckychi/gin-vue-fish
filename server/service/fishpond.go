package service

import (
	"fmt"
	"gin-vue-admin/global"
	"github.com/robfig/cron/v3"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"net/http"
)

type Info struct {
	Temp0 int64 `json:"temp0"`
	Hum int64 `json:"hum"`
	Temp1 int64 `json:"temp1"`
	Bmp int64 `json:"bmp"`
	Update_time string `json:"update_time"`
}

type FishPond struct {
	Id int `json:"id"`
	Info
}
var x Info
func GetFormOnenet()  {
	//var x Info

	req, _ := http.NewRequest("GET", "http://api.heclouds.com/devices/605690246/datastreams?datastream_ids=temp,hum,temp1,bmp", nil)
	// 设置个token
	req.Header.Set("api-key", "Lf=fP002oXgQzlbuY=AOjh7biA8=")
	// 再设置个json
	req.Header.Set("Content-Type","application/json")

	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		fmt.Println("query topic failed", err.Error())
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	value := gjson.Get(string(body), "data.#.current_value").Array()
	x.Temp0=value[0].Int()
	x.Hum=value[1].Int()
	x.Temp1=value[2].Int()
	x.Bmp=value[3].Int()
	value1 := gjson.Get(string(body), "data.1.update_at")
	x.Update_time=value1.Str
	err = global.GVA_DB.Table("fishpond1").Save(x).Error
	if err!=nil{
		fmt.Println("error is ",err)
	}
}

func updatePond(id int){
	var pond FishPond
	pond.Info=x
	pond.Id=id
	err := global.GVA_DB.Table("fish").Save(pond).Error
	if err!=nil{
		fmt.Println("error is ",err)
	}
}

func TimeTasks()  {
	//添加cron.WithSeconds()才能支持秒
	crontab := cron.New(cron.WithSeconds())
	task := func() {
		GetFormOnenet()
		updatePond(1)
	}
	// 添加定时任务, */5 * * * * ?是 crontab,表示每5秒执行一次
	crontab.AddFunc("*/5 * * * * ?", task)
	// 启动定时器
	crontab.Start()
}