package v1

import (
	"fmt"
	"gin-vue-admin/global/response"
	"gin-vue-admin/model"
	"gin-vue-admin/model/request"
	resp "gin-vue-admin/model/response"
	"gin-vue-admin/service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// @Tags Fish
// @Summary 创建Fish
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Fish true "创建Fish"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /fish/createFish [post]
func CreateFish(c *gin.Context) {
	var fish model.Fish
	_ = c.ShouldBindJSON(&fish)
	err := service.CreateFish(fish)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("创建失败，%v", err), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags Fish
// @Summary 删除Fish
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Fish true "删除Fish"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /fish/deleteFish [delete]
func DeleteFish(c *gin.Context) {
	var fish model.Fish
	_ = c.ShouldBindJSON(&fish)
	err := service.DeleteFish(fish)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("删除失败，%v", err), c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags Fish
// @Summary 更新Fish
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Fish true "更新Fish"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /fish/updateFish [put]
func UpdateFish(c *gin.Context) {
	var fish model.Fish
	_ = c.ShouldBindJSON(&fish)
	err := service.UpdateFish(&fish)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("更新失败，%v", err), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags Fish
// @Summary 用id查询Fish
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Fish true "用id查询Fish"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /fish/findFish [get]
func FindFish(c *gin.Context) {
	var fish model.Fish
	fish.ID,_=strconv.Atoi(c.Query("id"))
	//_ = c.ShouldBindQuery(&fish)
	fmt.Println("This is FindFish ID : ",fish.ID)
	err, refish := service.GetFish(fish.ID,"fish")
	fmt.Println("This is FindFish refish : ",refish)

	if err != nil {
		response.FailWithMessage(fmt.Sprintf("查询失败，%v", err), c)
	} else {
		//response.OkWithData(gin.H{"refish": refish}, c)
		c.JSON(http.StatusOK,refish)
	}
}

// @Tags Fish
// @Summary 分页获取Fish列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.FishSearch true "分页获取Fish列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /fish/getFishList [get]
func GetFishList(c *gin.Context) {
	var pageInfo request.FishSearch
	_ = c.ShouldBindQuery(&pageInfo)
	err, list, total := service.GetFishInfoList(pageInfo)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("获取数据失败，%v", err), c)
	} else {
		response.OkWithData(resp.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, c)
	}
}
//获取某一鱼塘的详细信息
func FindFishpond(c *gin.Context) {
	var fish model.Fish
	var err error
	//_ = c.ShouldBindQuery(&fish)
	fish.ID,err=strconv.Atoi(c.Query("id"))
	err, refish := service.GetFishpond(fish.ID)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("查询失败，%v", err), c)
	} else {
		//response.OkWithData(gin.H{"refish": refish}, c)
		//response.Result(200,refish,"str",c)
		c.JSON(http.StatusOK,refish)
	}
}
//获取某一鱼塘过去六小时的信息
func GetFishPondListByTime(c *gin.Context) {
	var fish[] model.Fish
	var err error
	id,err:=strconv.Atoi(c.Query("id"))
	err, fish = service.GetFishPondInfoByTime(id)
	fmt.Println("this is time : ",fish)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("查询失败，%v", err), c)
	} else {
		//response.OkWithData(gin.H{"refish": refish}, c)
		//response.Result(200,refish,"str",c)
		c.JSON(http.StatusOK,fish)
	}
}