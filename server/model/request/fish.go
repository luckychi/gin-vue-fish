package request

import "gin-vue-admin/model"

type FishSearch struct{
    model.Fish
    PageInfo
}