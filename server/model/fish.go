// 自动生成模板Fish
package model

// 如果含有time.Time 请自行import time包
type Fish struct {
      ID int `json:"id"`
      Update_time  string `json:"update_time" form:"update_time" gorm:"column:update_time;comment:'更新时间'"`
      Temp0  int `json:"temp0" form:"temp0" gorm:"column:temp0;comment:'气温'"`
      Bmp  int `json:"bmp" form:"bmp" gorm:"column:bmp;comment:'气压'"`
      Hum  int `json:"hum" form:"hum" gorm:"column:hum;comment:'湿度'"`
      Temp1  int `json:"temp1" form:"temp1" gorm:"column:temp1;comment:'水温'"`
      Status  bool `json:"status" form:"status" gorm:"column:status;comment:'增氧机状态'"`
      Tag string  `json:"tag"`
}
